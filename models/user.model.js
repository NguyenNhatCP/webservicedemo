var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
	email: { type: String, match: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/, unique: true, required: true },
    name: { type: String, required: true,trim: true, minlength: 2},
    password: { type: String, required: true, minlength: 6},
    password_confirm: {type: String, required: true, trim: true, minlength: 6},
    permission: { type: String, enum:['admin','customer'] },
    isBanned: { type: Number, default: 0 }, //1: banned
    date: { type: Date, default: Date.now }
   // categories: [{ type: Schema.Types.ObjectId, ref: "Category" }],
   // products: [{ type: Schema.Types.ObjectId, ref: "products" }],
    //posts: [{ type: Schema.Types.ObjectId, ref: "Post" }],
    //comments: [{ type: Schema.Types.ObjectId, ref: "Comment" }]
});

const User = mongoose.model('User',userSchema);

module.exports = User;