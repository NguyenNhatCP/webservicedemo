//var db = require('../db');
const User = require('../models/user.model');
var shortid = require('shortid');
const bcrypt = require('bcrypt');

module.exports.index = function(req,res){
	var pageData = {
  		title:'Account'
  }
	res.render('users/index',{
		pageData:pageData
		//users: db.get('users').value()
		});
};

module.exports.search = function(req,res){
	 var pageData = {
		  	title:'Result'
  }
	var q = req.query.q;
	var matchedUsers = db.get('users').value().filter(function(user)
	{
			return user.name.toLowerCase().indexOf(q.toLowerCase()) !== -1;
		});
	res.render('users/index',{
		pageData:pageData,
		users : matchedUsers
		})
};
/* check if email already exists in database */
const create = function (req,res,next){
	try
	{
		User.findOne({email: req.body.email},(err,user)=>
		{
			//check email has been exists yet
			if(user == null){
				bcrypt.hash(req.body.password,10, function(err,hash){
					if(err) {return next(err);}
					const newUser = new User(req.body);
					newUser.password = hash;
					newUser.save((err,result)=>{
						if(err) {return res.json(res.json({
							result: 'failed',
							message: `Register user failed. Error: ${err}`
						}))}
						res.json({
							result:'ok',
							message: 'Register user sucessfully!',
						})
					})
				})
			}
			else {
				res.json({
					result:'failed',
					message:'Email has been exists'
				})
			}
		})
	}
	catch(error){
		throw error;
	}
}
const createUser = async(req,res) => {
	var pageData = {
		title:'Create User',
	  	name: 'Form Register'
	}
	  res.render('users/create',{
		  pageData: pageData
	  });
  };
	/*
module.exports.get = function(req,res){
	var id = req.params.id;
	var user = db.get('users').find({id: id}).value();
	res.render('users/view',{
		user: user
	});
};

module.exports.postCreate = function(req,res){
	req.body.id = shortid.generate();
	req.body.avatar = req.file.path.split('/').slice(1).join('/');

	console.log(res.local);

	db.get('users').push(req.body).write();
	//Chuyển trang
	res.redirect('/users');
};
*/
module.exports ={create,createUser}